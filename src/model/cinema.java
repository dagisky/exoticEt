package model;

public class cinema {
	
	public int cinema_id; 
	public String cinema_name;
	public String profile_img;
	public Double Gps_lat; 
	public Double Gps_lng;  
	public int Gps_zom;  
	public String small_desc;    
	public String phone1; 
	public String phone2; 
	public String email;  
	public String website;  
	public String country; 
	public String city;  
	public String address_desc;

	public cinema(String[] cinema) {
		// TODO Auto-generated constructor stub
		this.cinema_id = cinema[0] == null || cinema[0].trim().equals("") ? 0 : Integer.parseInt(cinema[0]);
		this.cinema_name = cinema[1];
		this.profile_img = cinema[2];
		this.Gps_lat = cinema[3] == null || cinema[3].trim().equals("") ? 0 : Double.parseDouble(cinema[3]);
		this.Gps_lng = cinema[4] == null  || cinema[4].trim().equals("") ? 0 : Double.parseDouble(cinema[4]);
		this.Gps_zom = cinema[5] == null || cinema[5].trim().equals("") ? 0 : Integer.parseInt(cinema[5]);
		this.small_desc = cinema[6];
		this.phone1 = cinema[7];
		this.phone2 = cinema[8];
		this.email = cinema[9];
		this.website = cinema[10];
		this.country = cinema[11];
		this.city = cinema[12];
		this.address_desc = cinema[13];
	}

}
