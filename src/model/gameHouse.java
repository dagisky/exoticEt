package model;

public class gameHouse {
	
	public int game_house_id; 
	public String game_house_name;
	public String profile_img;
	public Double Gps_lat; 
	public Double Gps_lng;  
	public int Gps_zom;  
	public String small_desc;    
	public String phone1; 
	public String phone2; 
	public String email;  
	public String website;  
	public String country; 
	public String city;  
	public String address_desc;

	public gameHouse(String[] game_house) {
		// TODO Auto-generated constructor stub
		this.game_house_id = game_house[0] == null || game_house[0].trim().equals("") ? 0 : Integer.parseInt(game_house[0]);
		this.game_house_name = game_house[1];
		this.profile_img = game_house[2];
		this.Gps_lat = game_house[3] == null || game_house[3].trim().equals("") ? 0 : Double.parseDouble(game_house[3]);
		this.Gps_lng = game_house[4] == null  || game_house[4].trim().equals("") ? 0 : Double.parseDouble(game_house[4]);
		this.Gps_zom = game_house[5] == null || game_house[5].trim().equals("") ? 0 : Integer.parseInt(game_house[5]);
		this.small_desc = game_house[6];
		this.phone1 = game_house[7];
		this.phone2 = game_house[8];
		this.email = game_house[9];
		this.website = game_house[10];
		this.country = game_house[11];
		this.city = game_house[12];
		this.address_desc = game_house[13];
	}

}
