package model;

public class site {

	public int site_id;
	public String site_name;
	public String profile_img;
	public Double Gps_lat;
	public Double GPS_long;
	public int Gps_zoom;
	public int site_category;
	public String site_category_type;
	public String small_desc;
	public String phone1;
	public String phone2;
	public String email;
	public String website;
	public String country;
	public String city;
	public String address_desc;
	
	
	public site(String[] site) {
		// TODO Auto-generated constructor stub
		this.site_id = site[0] == null || site[0].trim().equals("") ? 0 : Integer.parseInt(site[0]);
		this.site_name = site[1];
		this.profile_img = site[2];
		this.Gps_lat = site[3] == null || site[3].trim().equals("")  ? 0 : Double.parseDouble(site[3]);
		this.GPS_long = site[4] == null || site[5].trim().equals("") ? 0 : Double.parseDouble(site[4]);
		this.Gps_zoom = site[5] == null || site[5].trim().equals("")? 0 :Integer.parseInt(site[5]);
		this.site_category = site[6] == null || site[6].trim().equals("")? 0 :Integer.parseInt(site[6]);
		this.site_category_type = site[7];
		this.small_desc = site[8];
		this.phone1 = site[9];
		this.phone2 = site[10];
		this.email = site[11];
		this.website = site[12];
		this.country = site[13];
		this.city = site[14];
		this.address_desc = site[15];
	}
	
	public site(){}

}
