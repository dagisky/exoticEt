package model;

public class resort {

	public int resort_id;
	public String resort_name;
	public String profile_img;
	public double Gps_lat;
	public double GPS_long;
	public int Gps_zoom;
	public String small_desc;
	public String phone1;
	public String phone2;
	public String email;
	public String website;
	public String country;
	public String city;
	public String address_desc;
	
	public resort(String[] resort) {
		// TODO Auto-generated constructor stub
		this.resort_id = resort[0] == null || resort[0].trim().equals("") ? 0 : Integer.parseInt(resort[0]);
		this.resort_name = resort[1];
		this.profile_img = resort[2];
		this.Gps_lat = resort[3] == null || resort[3].trim().equals("") ? 0 : Double.parseDouble(resort[3]);
		this.GPS_long = resort[4] == null  || resort[4].trim().equals("") ? 0 : Double.parseDouble(resort[4]);
		this.Gps_zoom = resort[5] == null || resort[5].trim().equals("") ? 0 : Integer.parseInt(resort[5]);
		this.small_desc = resort[6];
		this.phone1 = resort[7];
		this.phone2 = resort[8];
		this.email = resort[9];
		this.website = resort[10];
		this.country = resort[11];
		this.city = resort[12];
		this.address_desc = resort[13];
	}

}
