package model;

public class guestHouse {
	public int guest_house_id;
	public String guest_house_name;
	public String profile_img;
	public double Gps_lat;
	public double Gps_long;
	public int Gps_zoom;
	public String small_desc;
	public String phone1;
	public String phone2;
	public String email;
	public String website;
	public String country;
	public String city;
	public String address_desc;

	public guestHouse(String[] guestHouse) {
		// TODO Auto-generated constructor stub
		this.guest_house_id = guestHouse[0] == null || guestHouse[0].trim().equals("") ? 0 : Integer.parseInt(guestHouse[0]); 
		this.guest_house_name = guestHouse[1];
		this.profile_img = guestHouse[2];
		this.Gps_lat = guestHouse[3] == null || guestHouse[3].trim().equals("")? 0 :Double.parseDouble(guestHouse[3]);
		this.Gps_long = guestHouse[4] == null || guestHouse[4].trim().equals("")? 0 :Double.parseDouble(guestHouse[4]);
		this.Gps_zoom = guestHouse[5] == null || guestHouse[5].trim().equals("")? 0 :Integer.parseInt(guestHouse[5]);
		this.small_desc = guestHouse[6];
		this.phone1 = guestHouse[7];
		this.phone2 = guestHouse[8];
		this.email = guestHouse[9];
		this.website = guestHouse[10];
		this.country = guestHouse[11];
		this.city = guestHouse[12];
		this.address_desc = guestHouse[13];
	}

}
