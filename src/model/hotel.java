package model;

public class hotel {
	public int hotel_id;
	public String hotel_name;
	public String profile_img;
	public int star;
	public Double Gps_lat;
	public Double GPS_long;
	public int Gps_zoom;
	public String small_desc;
	public String phone1;
	public String phone2;
	public String email;
	public String website;
	public String country;
	public String city;
	public String address_desc;
	
	public hotel(String[] hotel) {
		// TODO Auto-generated constructor stub
		this.hotel_id = hotel[0] == null || hotel[0].trim().equals("") ? 0 : Integer.parseInt(hotel[0]);
		this.hotel_name = hotel[1];
		this.profile_img = hotel[2];
		this.star = hotel[3] == null || hotel[3].trim().equals("")? 0 : Integer.parseInt(hotel[3]);
		this.Gps_lat = hotel[4] == null || hotel[4].trim().equals("") ? 0 : Double.parseDouble(hotel[4]);
		this.GPS_long = hotel[5] == null || hotel[5].trim().equals("")? 0 : Double.parseDouble(hotel[5]);
		this.Gps_zoom = hotel[6] == null || hotel[6].trim().equals("") ? 0 : Integer.parseInt(hotel[6]);
		this.small_desc = hotel[7];
		this.phone1 = hotel[8];
		this.phone2 = hotel[9];
		this.email = hotel[10];
		this.website = hotel[11];
		this.country = hotel[12];
		this.city = hotel[13];
		this.address_desc = hotel[14];
	}

}
