package com.example.exoticet;

import java.util.ArrayList;
import java.util.List;

import model.cinema;
import model.gameHouse;
import model.guestHouse;
import model.hotel;
import model.resort;
import model.site;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class dbHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "exoticethiopia";
	private static final int DTABASE_VERSION = 1;
	private Context context;
	
	public dbHelper(Context context) {
		super(context, DATABASE_NAME, null, DTABASE_VERSION);
		// TODO Auto-generated constructor stub
		this.context = context;	
		
	}
	
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String query[] = dbTables.getSql();
		try {
			for(int i = 0; i < query.length; i++)
			db.execSQL(query[i]);
			
		} catch (SQLException e) {
			Toast.makeText(context, "Error: " + e, Toast.LENGTH_SHORT).show();
		}
	}
	
	

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {		
		
	}
	
	public void updatedb(String query[]){
		SQLiteDatabase db = this.getWritableDatabase();
		try {
		for(int i = 0; i < query.length; i++)
			db.execSQL(query[i]);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public List<hotel> getAllHotels(){
		SQLiteDatabase db = this.getReadableDatabase();
		List<hotel> hotels = new ArrayList<hotel>();
				
		String query = "SELECT * FROM hotel";
		Cursor cursor = db.rawQuery(query, null);
				
		if(cursor.moveToFirst()){
			do{	
				String[] tempStore = { String.valueOf(cursor.getInt(0)), cursor.getString(1),
									  cursor.getString(2), String.valueOf(cursor.getInt(3)), String.valueOf(cursor.getDouble(4)),
									  String.valueOf(cursor.getDouble(5)),String.valueOf(cursor.getInt(6)),cursor.getString(7),
									  cursor.getString(8), cursor.getString(9),cursor.getString(10), cursor.getString(11),
									  cursor.getString(12), cursor.getString(13), cursor.getString(14)}; 
				
				hotels.add(new hotel(tempStore));
				
			}while(cursor.moveToNext());
		}
		db.close();
		return hotels;
	}
	
	
	public List<resort> getAllResorts(){
		SQLiteDatabase db = this.getReadableDatabase();
		List<resort> resorts = new ArrayList<resort>();
				
		String query = "SELECT * FROM resort";
		Cursor cursor = db.rawQuery(query, null);
				
		if(cursor.moveToFirst()){
			do{	
				String[] tempStore = {String.valueOf(cursor.getInt(0)), cursor.getString(1),
						  cursor.getString(2), String.valueOf(cursor.getDouble(3)), String.valueOf(cursor.getDouble(4)),
						  String.valueOf(cursor.getInt(5)),cursor.getString(6), cursor.getString(7), cursor.getString(8),
						  cursor.getString(9), cursor.getString(10),cursor.getString(11), cursor.getString(12), cursor.getString(13)}; 
				
				resorts.add(new resort(tempStore));
				
			}while(cursor.moveToNext());
		}
		db.close();
		return resorts;
	}
	
	public List<guestHouse> getAllGuestHouse(){
		SQLiteDatabase db = this.getReadableDatabase();
		List<guestHouse> guestHouse = new ArrayList<guestHouse>();
				
		String query = "SELECT * FROM guest_house";
		Cursor cursor = db.rawQuery(query, null);
				
		if(cursor.moveToFirst()){
			do{	
				String[] tempStore = {String.valueOf(cursor.getInt(0)), cursor.getString(1),cursor.getString(2),
						  String.valueOf(cursor.getDouble(3)), String.valueOf(cursor.getDouble(4)),
						  String.valueOf(cursor.getInt(5)), cursor.getString(6), cursor.getString(7), cursor.getString(8),
						  cursor.getString(9), cursor.getString(10),cursor.getString(11) ,cursor.getString(12), cursor.getString(13)}; 
				
				guestHouse.add(new guestHouse(tempStore));
				
			}while(cursor.moveToNext());
		}
		db.close();
		return guestHouse;
	}
	
	public List<cinema> getAllCinema(){
		SQLiteDatabase db = this.getReadableDatabase();
		List<cinema> cinemas = new ArrayList<cinema>();
				
		String query = "SELECT * FROM cinema";
		Cursor cursor = db.rawQuery(query, null);
				
		if(cursor.moveToFirst()){
			do{	
				String[] tempStore = {String.valueOf(cursor.getInt(0)), cursor.getString(1),cursor.getString(2),
						  String.valueOf(cursor.getDouble(3)), String.valueOf(cursor.getDouble(4)),
						  String.valueOf(cursor.getInt(5)), cursor.getString(6), cursor.getString(7), cursor.getString(8),
						  cursor.getString(9), cursor.getString(10),cursor.getString(11) ,cursor.getString(12), cursor.getString(13)}; 
				
				cinemas.add(new cinema(tempStore));
				
			}while(cursor.moveToNext());
		}
		db.close();
		return cinemas;
	}
	
	public List<gameHouse> getAllGameHouse(){
		SQLiteDatabase db = this.getReadableDatabase();
		List<gameHouse> gameHs = new ArrayList<gameHouse>();
				
		String query = "SELECT * FROM game_house";
		Cursor cursor = db.rawQuery(query, null);
				
		if(cursor.moveToFirst()){
			do{	
				String[] tempStore = {String.valueOf(cursor.getInt(0)), cursor.getString(1),cursor.getString(2),
						  String.valueOf(cursor.getDouble(3)), String.valueOf(cursor.getDouble(4)),
						  String.valueOf(cursor.getInt(5)), cursor.getString(6), cursor.getString(7), cursor.getString(8),
						  cursor.getString(9), cursor.getString(10),cursor.getString(11) ,cursor.getString(12), cursor.getString(13)}; 
				
				gameHs.add(new gameHouse(tempStore));
				
			}while(cursor.moveToNext());
		}
		db.close();
		return gameHs;
	}
	
	
	public List<site> getAllSites(int site_category_id){
		SQLiteDatabase db = this.getReadableDatabase();
		List<site> sites = new ArrayList<site>();
				
		String query = "SELECT * FROM touristic_site WHERE site_category = " + site_category_id;
		Cursor cursor = db.rawQuery(query, null);
				
		if(cursor.moveToFirst()){
			do{	
				
				String[] tempStore = { String.valueOf(cursor.getInt(0)), cursor.getString(1),
									  cursor.getString(2), String.valueOf(cursor.getDouble(3)), String.valueOf(cursor.getDouble(4)),
									  String.valueOf(cursor.getInt(5)),String.valueOf(cursor.getInt(6)),cursor.getString(7),
									  cursor.getString(8), cursor.getString(9),cursor.getString(10), cursor.getString(11),
									  cursor.getString(12), cursor.getString(13), cursor.getString(14), cursor.getString(15)};
				
				
				sites.add(new site(tempStore));
				
				
			}while(cursor.moveToNext());
			db.close();
			return sites;
		}else{
			return null;
		}
		
		
	}
	
	


}
