package com.example.exoticet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import model.guestHouse;
import model.hotel;
import model.resort;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.internal.http.HttpConnection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;

public class Accommodation_list extends Activity {

	ListView hotelListView;
	ListView resortsListView;
	ListView guestHouseListView;
	TabHost th;

	String path;

	hotel clickedHotel = null;
	resort clickedResort = null;
	guestHouse clickedguestHouse = null;
	
	OkHttpClient client = new OkHttpClient();

	public Accommodation_list() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.accommodation_list);

		th = (TabHost) findViewById(R.id.tabHostAccom);
		th.setup();

		hotelListView = (ListView) findViewById(R.id.lvHotels);
		resortsListView = (ListView) findViewById(R.id.lvResorts);
		guestHouseListView = (ListView) findViewById(R.id.lvGuestHouse);

		MyTask fetchData = new MyTask();
		fetchData.execute();
	}

	/*
	 * =========================================UPDATE VIEW
	 * FUNCTIONALITY===============================================
	 */

	private void updateView(final List<hotel> hotels,
			final List<resort> resorts, final List<guestHouse> guestHouses) {

		if (hotels != null) {
			hotelAdapter adapter = new hotelAdapter(getBaseContext(),
					android.R.layout.simple_list_item_1, R.id.tvName1, hotels);
			hotelListView.setAdapter(adapter);
		}
		if (resorts != null) {
			ResortAdapter adapter2 = new ResortAdapter(getBaseContext(),
					android.R.layout.simple_list_item_1, R.id.tvName1, resorts);
			resortsListView.setAdapter(adapter2);
		}
		if (guestHouses != null) {
			GuestHAdapter adapter3 = new GuestHAdapter(getBaseContext(),
					android.R.layout.simple_list_item_1, R.id.tvName1,
					guestHouses);
			guestHouseListView.setAdapter(adapter3);
		}

		TabSpec ts = th.newTabSpec("tag1");
		ts.setContent(R.id.hotels);
		ts.setIndicator("Hotels");
		th.addTab(ts);

		ts = th.newTabSpec("tag2");
		ts.setContent(R.id.resorts);
		ts.setIndicator("Resorts");
		th.addTab(ts);

		ts = th.newTabSpec("tag3");
		ts.setContent(R.id.guestHouses);
		ts.setIndicator("Guest House");
		th.addTab(ts);

		registerForContextMenu(hotelListView);
		registerForContextMenu(resortsListView);
		registerForContextMenu(guestHouseListView);

		hotelListView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View view,
					int i, long l) {
				// openContextMenu(view);

				clickedHotel = hotels.get(i);
				clickedguestHouse = null;
				clickedResort = null;
				return false;
			}
		});

		resortsListView
				.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> adapter,
							View view, int i, long l) {
						// TODO Auto-generated method stub
						clickedHotel = null;
						clickedguestHouse = null;
						clickedResort = resorts.get(i);
						return false;
					}
				});

		guestHouseListView
				.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> adapter,
							View view, int i, long l) {
						// TODO Auto-generated method stub
						clickedHotel = null;
						clickedguestHouse = guestHouses.get(i);
						clickedResort = null;
						return false;
					}
				});
		hotelListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int i,
					long l) {
				Intent intent = new Intent(getBaseContext(), Detail_view.class);
				intent.putExtra("TYPE", "hotel");
				String hotel[] = { String.valueOf(hotels.get(i).hotel_id),
						hotels.get(i).hotel_name, hotels.get(i).profile_img,
						String.valueOf(hotels.get(i).star),
						String.valueOf(hotels.get(i).Gps_lat),
						String.valueOf(hotels.get(i).GPS_long),
						String.valueOf(hotels.get(i).Gps_zoom),
						hotels.get(i).small_desc, hotels.get(i).phone1,
						hotels.get(i).phone2, hotels.get(i).email,
						hotels.get(i).website, hotels.get(i).country,
						hotels.get(i).city, hotels.get(i).address_desc };
				intent.putExtra("hotel", hotel);
				intent.putExtra("ID", hotels.get(i).hotel_id);
				startActivity(intent);

			}
		});

		guestHouseListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int i,
					long l) {
				Intent intent = new Intent(getBaseContext(), Detail_view.class);
				intent.putExtra("TYPE", "guest_house");
				String guest_house[] = {
						String.valueOf(guestHouses.get(i).guest_house_id),
						guestHouses.get(i).guest_house_name,
						guestHouses.get(i).profile_img,
						String.valueOf(guestHouses.get(i).Gps_lat),
						String.valueOf(guestHouses.get(i).Gps_long),
						String.valueOf(guestHouses.get(i).Gps_zoom),
						guestHouses.get(i).small_desc,
						guestHouses.get(i).phone1, guestHouses.get(i).phone2,
						guestHouses.get(i).email, guestHouses.get(i).website,
						guestHouses.get(i).country, guestHouses.get(i).city,
						guestHouses.get(i).address_desc };
				intent.putExtra("guest_house", guest_house);
				startActivity(intent);

			}
		});

		resortsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View veiw, int i,
					long l) {
				Intent intent = new Intent(getBaseContext(), Detail_view.class);
				intent.putExtra("TYPE", "resort");
				String resort[] = { String.valueOf(resorts.get(i).resort_id),
						resorts.get(i).resort_name, resorts.get(i).profile_img,
						String.valueOf(resorts.get(i).Gps_lat),
						String.valueOf(resorts.get(i).GPS_long),
						String.valueOf(resorts.get(i).Gps_zoom),
						resorts.get(i).small_desc, resorts.get(i).phone1,
						resorts.get(i).phone2, resorts.get(i).email,
						resorts.get(i).website, resorts.get(i).country,
						resorts.get(i).city, resorts.get(i).address_desc };
				intent.putExtra("resort", resort);

				startActivity(intent);

			}
		});

	}

	/*
	 * =====================================ON CONTEXT MENU
	 * CREATED=========================================
	 */

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub

		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle("More Options...");
		menu.add("View Map");
		menu.add("Dial Phone 1");
		menu.add("Dial Phone 2");
		menu.add("Go to Website");
	}

	/*
	 * ======================================ON CONTEXT MENU ITEM
	 * SELECTED=============================================
	 */

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		if (item.getTitle() == "View Map") {
			if (clickedHotel != null) {
				Intent mapIntent = null, chooser = null;
				mapIntent = new Intent(android.content.Intent.ACTION_VIEW);
				mapIntent.setData(Uri
						.parse("geo:" + clickedHotel.Gps_lat + ","
								+ clickedHotel.GPS_long + "?z="
								+ clickedHotel.Gps_zoom));
				chooser = Intent.createChooser(mapIntent, "Launch Maps");
				startActivity(chooser);
			} else if (clickedResort != null) {
				Intent mapIntent = null, chooser = null;
				mapIntent = new Intent(android.content.Intent.ACTION_VIEW);
				mapIntent.setData(Uri.parse("geo:" + clickedResort.Gps_lat
						+ "," + clickedResort.GPS_long + "?z="
						+ clickedResort.Gps_zoom));
				chooser = Intent.createChooser(mapIntent, "Launch Maps");
				startActivity(chooser);
			} else if (clickedguestHouse != null) {
				Intent mapIntent = null, chooser = null;
				mapIntent = new Intent(android.content.Intent.ACTION_VIEW);
				mapIntent.setData(Uri.parse("geo:" + clickedguestHouse.Gps_lat
						+ "," + clickedguestHouse.Gps_long + "?z="
						+ clickedguestHouse.Gps_zoom));
				chooser = Intent.createChooser(mapIntent, "Launch Maps");
				startActivity(chooser);
			}
			return true;
		} else if (item.getTitle() == "Dial Phone 1") {
			if (clickedHotel != null) {
				Uri number = Uri
						.parse("tel:" + Uri.encode(clickedHotel.phone1));
				Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
				startActivity(callIntent);
			} else if (clickedResort != null) {
				Uri number = Uri.parse("tel:"
						+ Uri.encode(clickedResort.phone1));
				Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
				startActivity(callIntent);
			} else if (clickedguestHouse != null) {
				Uri number = Uri.parse("tel:"
						+ Uri.encode(clickedguestHouse.phone1));
				Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
				startActivity(callIntent);
			}
			return true;
		} else if (item.getTitle() == "Dial Phone 2") {
			if (clickedHotel != null) {
				Uri number = Uri
						.parse("tel:" + Uri.encode(clickedHotel.phone2));
				Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
				startActivity(callIntent);
			} else if (clickedResort != null) {
				Uri number = Uri.parse("tel:"
						+ Uri.encode(clickedResort.phone2));
				Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
				startActivity(callIntent);
			} else if (clickedguestHouse != null) {
				Uri number = Uri.parse("tel:"
						+ Uri.encode(clickedguestHouse.phone2));
				Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
				startActivity(callIntent);
			}
			return true;
		} else if (item.getTitle() == "Go to Website") {
			if (clickedHotel != null) {
				Uri webpage = Uri.parse(clickedHotel.website);
				Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
				startActivity(webIntent);
			} else if (clickedResort != null) {
				Uri webpage = Uri.parse(clickedResort.website);
				Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
				startActivity(webIntent);
			} else if (clickedguestHouse != null) {
				Uri webpage = Uri.parse(clickedguestHouse.website);
				Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
				startActivity(webIntent);
			}
			return true;
		} else {
			return false;
		}

	}

	/*
	 * ==========================================================================
	 * =================
	 */

	protected boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * ===========================HOTEL LIST
	 * ADAPTER========================================
	 */

	private class hotelAdapter extends ArrayAdapter<hotel> {

		private List<hotel> hotels;

		public hotelAdapter(Context context, int resource,
				int textViewResourceId, List<hotel> hotels) {
			super(context, resource, textViewResourceId, hotels);
			this.hotels = hotels;
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View row = inflator.inflate(R.layout.custom_list_1, parent, false);

			ImageView iv = (ImageView) row.findViewById(R.id.ivProfile1);
			TextView name = (TextView) row.findViewById(R.id.tvName1);
			TextView phone = (TextView) row.findViewById(R.id.tvPhone1);
			TextView mail = (TextView) row.findViewById(R.id.tvMail1);
			TextView address = (TextView) row.findViewById(R.id.tvAddress1);

			HotelsAndView hotelAndView = new HotelsAndView();
			hotelAndView.ohotel = hotels.get(position);
			hotelAndView.view = row;
			HotelImageLoader loader = new HotelImageLoader();
			if (isOnline()) {
				loader.execute(hotelAndView);
			} else {
				Toast.makeText(getBaseContext(), "No Internet Connection",
						Toast.LENGTH_LONG).show();
			}

			name.setText(hotels.get(position).hotel_name);
			phone.setText(hotels.get(position).phone1 + ", "
					+ hotels.get(position).phone2);
			mail.setText(hotels.get(position).email);
			address.setText(hotels.get(position).address_desc);

			// the hotel image loader

			return row;
		}
	}

	/*
	 * ==========================================================================
	 * ================================
	 */

	private class ResortAdapter extends ArrayAdapter<resort> {

		private List<resort> resorts;

		public ResortAdapter(Context context, int resource,
				int textViewResourceId, List<resort> resorts) {
			super(context, resource, textViewResourceId, resorts);
			this.resorts = resorts;
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View row = inflator.inflate(R.layout.custom_list_1, parent, false);

			ImageView iv = (ImageView) row.findViewById(R.id.ivProfile1);
			TextView name = (TextView) row.findViewById(R.id.tvName1);
			TextView phone = (TextView) row.findViewById(R.id.tvPhone1);
			TextView mail = (TextView) row.findViewById(R.id.tvMail1);
			TextView address = (TextView) row.findViewById(R.id.tvAddress1);

			name.setText(resorts.get(position).resort_name);
			phone.setText(resorts.get(position).phone1 + ", "
					+ resorts.get(position).phone2);
			mail.setText(resorts.get(position).email);
			address.setText(resorts.get(position).address_desc);

			return row;
		}
	}

	/*
	 * ==========================================================================
	 * ================================
	 */

	private class GuestHAdapter extends ArrayAdapter<guestHouse> {

		private List<guestHouse> guestHs;

		public GuestHAdapter(Context context, int resource,
				int textViewResourceId, List<guestHouse> guestHs) {
			super(context, resource, textViewResourceId, guestHs);
			this.guestHs = guestHs;
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View row = inflator.inflate(R.layout.custom_list_1, parent, false);

			ImageView iv = (ImageView) row.findViewById(R.id.ivProfile1);
			TextView name = (TextView) row.findViewById(R.id.tvName1);
			TextView phone = (TextView) row.findViewById(R.id.tvPhone1);
			TextView mail = (TextView) row.findViewById(R.id.tvMail1);
			TextView address = (TextView) row.findViewById(R.id.tvAddress1);

			name.setText(guestHs.get(position).guest_house_name);
			phone.setText(guestHs.get(position).phone1 + " "
					+ guestHs.get(position).phone2);
			mail.setText(guestHs.get(position).email);
			address.setText(guestHs.get(position).address_desc);

			return row;
		}
	}

	/*
	 * ==========================================================================
	 * ================================
	 */

	private class MyTask extends AsyncTask<String, String, String> {

		List<hotel> lhotels;
		List<resort> lresorts;
		List<guestHouse> lguestHouses;

		@Override
		protected String doInBackground(String... arg0) {
			dbHelper db = new dbHelper(getBaseContext());

			lhotels = db.getAllHotels();
			lresorts = db.getAllResorts();
			lguestHouses = db.getAllGuestHouse();
			return null;
		}

		@Override
		protected void onPostExecute(String result) {

			updateView(lhotels, lresorts, lguestHouses);			

		}
	}

	/*
	 * ==========================================================================
	 * ==========
	 */

	private class HotelsAndView {
		public hotel ohotel;
		public View view;
		public Bitmap bitmap;
	}

	private class ResortsAndView {
		public resort oresort;
		public View view;
		public Bitmap bitmap;
	}

	private class GuestHousesAndView {
		public guestHouse oguestHouse;
		public View view;
		public Bitmap bitmap;
	}

	/*
	 * ==========================================================================
	 * =============
	 */

	private class HotelImageLoader extends
			AsyncTask<HotelsAndView, Void, HotelsAndView> {

		@Override
		protected HotelsAndView doInBackground(HotelsAndView... params) {
			HotelsAndView container = params[0];
			hotel htl = container.ohotel;

			String imgUrl = htl.profile_img;
			try {
				HttpURLConnection con = client.open(new URL(imgUrl));
				InputStream in = con.getInputStream();
				
				Bitmap bitmap = BitmapFactory.decodeStream(in);
				container.bitmap = bitmap;
				in.close();
				return container;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(HotelsAndView result) {
			ImageView iv = (ImageView) result.view
					.findViewById(R.id.ivProfile1);
			iv.setImageBitmap(result.bitmap);

			fileSt store = new fileSt(Environment.getExternalStorageDirectory()
					.toString());
			/* ======================write data=========================== */
			if (store.chkExternalStorage(getBaseContext())) {
				
					try {
						path = store.writeImgExt(
								openFileOutput(result.ohotel.hotel_name+"_"+String.valueOf(result.ohotel.hotel_id), MODE_PRIVATE),
								result.bitmap,							
								result.ohotel.hotel_name + "_"
										+ String.valueOf(result.ohotel.hotel_id));
						Toast.makeText(getBaseContext(), path, Toast.LENGTH_LONG).show();
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			} else {
				
					try {
						boolean fc = store.writeImgInt(
								openFileOutput(result.ohotel.hotel_name+"_"+String.valueOf(result.ohotel.hotel_id), MODE_PRIVATE),
								result.bitmap,														
								result.ohotel.hotel_name + "_"
										+ String.valueOf(result.ohotel.hotel_id));
						if(fc)
							Toast.makeText(getBaseContext(), "file created in external storage", Toast.LENGTH_SHORT).show();
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			}
			/* ============================================================ */
		}

	}

	private class ResortImageLoader extends
			AsyncTask<ResortsAndView, Void, ResortsAndView> {

		@Override
		protected ResortsAndView doInBackground(ResortsAndView... arg0) {
			// TODO Auto-generated method stub
			return null;
		}

	}

	private class GuestHouseImageLoader extends
			AsyncTask<GuestHousesAndView, Void, GuestHousesAndView> {

		@Override
		protected GuestHousesAndView doInBackground(GuestHousesAndView... arg0) {
			// TODO Auto-generated method stub
			return null;
		}

	}

}
