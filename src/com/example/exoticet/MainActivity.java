package com.example.exoticet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnItemClickListener {

	ListView mainMenu;
	MyAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mainMenu = (ListView) findViewById(R.id.lvMenu);
		adapter = new MyAdapter(getBaseContext(),
				android.R.layout.simple_expandable_list_item_1,
				R.id.tvMainMenu, getResources()
						.getStringArray(R.array.mainMenu));

		mainMenu.setAdapter(adapter);
		mainMenu.setOnItemClickListener(this);
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int i, long l) {
		// TODO Auto-generated method stub

		try {
			Intent myintent;
			Class<?> link = Class.forName("com.example.exoticet.MainActivity");

			if (i == 0) {
				link = Class
						.forName("com.example.exoticet.Touristic_site_list");
			} else if (i == 1) {
				link = Class.forName("com.example.exoticet.Accommodation_list");
			} else if (i == 2) {
				link = Class.forName("com.example.exoticet.Entertainment_list");
			} else if (i == 5) {
				link = Class.forName("com.example.exoticet.Setting");

			} else if (i == 6) {
				link = Class.forName("com.example.exoticet.MainActivity");
			}

			myintent = new Intent(MainActivity.this, link);
			startActivity(myintent);

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			Toast.makeText(getBaseContext(), "Could Not Find Activity",
					Toast.LENGTH_SHORT).show();
		}
	}

	public class MyAdapter extends ArrayAdapter<String> {

		public MyAdapter(Context context, int resource, int textViewResourceId,
				String[] strings) {
			super(context, resource, textViewResourceId, strings);
			// TODO Auto-generated constructor stub

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View row = inflator.inflate(R.layout.custom_menu, parent, false);
			String[] items = getResources().getStringArray(R.array.mainMenu);
			String[] itemDesc = getResources().getStringArray(
					R.array.mainMenuDesc);

			TextView title = (TextView) row.findViewById(R.id.tvMainMenu);
			TextView desc = (TextView) row.findViewById(R.id.tvMainMenuDesc);

			title.setText(items[position]);
			desc.setText(itemDesc[position]);

			return row;
		}

	}
	
	

}
