package com.example.exoticet;

import model.cinema;
import model.gameHouse;
import model.guestHouse;
import model.hotel;
import model.resort;
import model.site;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class Detail_view extends Activity {
	
	private String type;
	

	public Detail_view() {
		// TODO Auto-generated constructor stub
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_view);
		
		TextView title = (TextView) findViewById(R.id.tvMainTitle);
		TextView phone = (TextView) findViewById(R.id.tvPhone);
		TextView mail = (TextView) findViewById(R.id.tvMail);
		TextView addressDesc = (TextView) findViewById(R.id.tvAddressDesc);
		TextView tvDesc = (TextView) findViewById(R.id.tvLargeDesc);
		
		
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		     		    
		    if(extras.getString("TYPE").trim().equals("game_house")){
		    	gameHouse gameH = new gameHouse(extras.getStringArray("game_house"));
		    	title.setText(gameH.game_house_name);
		    	phone.setText(gameH.phone1+", "+gameH.phone2);
		    	mail.setText(gameH.email);
		    	addressDesc.setText(gameH.address_desc);
		    	tvDesc.setText(gameH.small_desc);		    	
		    }else if(extras.getString("TYPE").trim().equals("cinema")){
		    	cinema cin = new cinema(extras.getStringArray("cinema"));
		    	title.setText(cin.cinema_name);
		    	phone.setText(cin.phone1+", "+cin.phone2);
		    	mail.setText(cin.email);
		    	addressDesc.setText(cin.address_desc);
		    	tvDesc.setText(cin.small_desc);
		    }else if(extras.getString("TYPE").trim().equals("hotel")){
		    	hotel hot = new hotel(extras.getStringArray("hotel"));
		    	title.setText(hot.hotel_name);
		    	phone.setText(hot.phone1+", "+hot.phone2);
		    	mail.setText(hot.email);
		    	addressDesc.setText(hot.address_desc);
		    	tvDesc.setText(hot.small_desc);
		    }else if(extras.getString("TYPE").trim().equals("resort")){
		    	resort res = new resort(extras.getStringArray("resort"));
		    	title.setText(res.resort_name);
		    	phone.setText(res.phone1);
		    	mail.setText(res.email);
		    	addressDesc.setText(res.address_desc);
		    	tvDesc.setText(res.small_desc);
		    }else if(extras.getString("TYPE").trim().equals("guest_house")){
		    	guestHouse guest_house = new guestHouse(extras.getStringArray("guest_house"));
		    	title.setText(guest_house.guest_house_name);
		    	phone.setText(guest_house.phone1+", "+guest_house.phone2);
		    	mail.setText(guest_house.email);
		    	addressDesc.setText(guest_house.address_desc);
		    	tvDesc.setText(guest_house.small_desc);
		    }else if(extras.getString("TYPE").trim().equals("site")){
		    	site tsite = new site(extras.getStringArray("site"));
		    	title.setText(tsite.site_name);
		    	phone.setText(tsite.phone1+", "+tsite.phone2);
		    	mail.setText(tsite.email);
		    	addressDesc.setText(tsite.address_desc);
		    	tvDesc.setText(tsite.small_desc);
		    }else{
		    	//kill the activity
		    }
		    
		}else{
			Toast.makeText(this.getBaseContext(), "no extras", Toast.LENGTH_LONG).show();
			
		}
		
	}

}
