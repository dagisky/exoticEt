package com.example.exoticet;

import java.util.List;

import model.cinema;
import model.gameHouse;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;

public class Entertainment_list extends Activity {
	
	ListView cinemaListView;
	ListView gameHouseListView;
	TabHost th;
	cinema clickedCinema;
	gameHouse clickedGameHouse;

	public Entertainment_list() {
		// TODO Auto-generated constructor stub
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.entertainment_list);

		th = (TabHost) findViewById(R.id.tabHostEntertain);
		th.setup();

		cinemaListView = (ListView) findViewById(R.id.lvCinema);
		gameHouseListView = (ListView) findViewById(R.id.lvGameHouse);
				
		MyTask fetchData = new MyTask();
		fetchData.execute();
	}
	
	
	/*=============================UPDATE VIEW====================================*/
	
	private void updateView(final List<cinema> cinemas, final List<gameHouse> gameHouses) {

		if (cinemas != null) {
			cinemaAdapter adapter = new cinemaAdapter(getBaseContext(),
					android.R.layout.simple_list_item_1, R.id.tvName1, cinemas);
			cinemaListView.setAdapter(adapter);
		}
		
		if (gameHouses != null) {
			gameHouseAdapter adapter2 = new gameHouseAdapter(getBaseContext(),
					android.R.layout.simple_list_item_1, R.id.tvName1, gameHouses);
			gameHouseListView.setAdapter(adapter2);
		}
		

		TabSpec ts = th.newTabSpec("tag1");
		ts.setContent(R.id.tabCinema);
		ts.setIndicator("Cinema");
		th.addTab(ts);

		ts = th.newTabSpec("tag2");
		ts.setContent(R.id.tabGameHouse);
		ts.setIndicator("Game House");
		th.addTab(ts);
		
		registerForContextMenu(cinemaListView);
		registerForContextMenu(gameHouseListView);
		
		cinemaListView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View view,
					int i, long l) {
				// TODO Auto-generated method stub
				clickedCinema = cinemas.get(i);
				clickedGameHouse = null;
				return false;
			}
		});
		
		gameHouseListView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View view,
					int i, long l) {
				// TODO Auto-generated method stub
				clickedGameHouse = gameHouses.get(i);
				clickedCinema = null;
				return false;
			}
		});
		
		gameHouseListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int i,
					long l) {
				
				Intent intent = new Intent(getBaseContext(), Detail_view.class);
				intent.putExtra("TYPE", "game_house");
				String game_house[] = {String.valueOf(gameHouses.get(i).game_house_id), gameHouses.get(i).game_house_name,
						gameHouses.get(i).profile_img, String.valueOf(gameHouses.get(i).Gps_lat), String.valueOf(gameHouses.get(i).Gps_lng),
						String.valueOf(gameHouses.get(i).Gps_zom),gameHouses.get(i).small_desc, gameHouses.get(i).phone1, gameHouses.get(i).phone2,
						gameHouses.get(i).email, gameHouses.get(i).website, gameHouses.get(i).country, gameHouses.get(i).city, gameHouses.get(i).address_desc};
				intent.putExtra("game_house", game_house);
				intent.putExtra("ID", gameHouses.get(i).game_house_id);
				startActivity(intent);
				
			}
		});
		
		cinemaListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int i,
					long l) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getBaseContext(), Detail_view.class);
				intent.putExtra("TYPE", "cinema");
				String cinema[] = {String.valueOf(cinemas.get(i).cinema_id), cinemas.get(i).cinema_name,
						cinemas.get(i).profile_img, String.valueOf(cinemas.get(i).Gps_lat), String.valueOf(cinemas.get(i).Gps_lng),
						String.valueOf(cinemas.get(i).Gps_zom),cinemas.get(i).small_desc, cinemas.get(i).phone1, cinemas.get(i).phone2,
						cinemas.get(i).email, cinemas.get(i).website, cinemas.get(i).country, cinemas.get(i).city, cinemas.get(i).address_desc};
				intent.putExtra("cinema", cinema);
				intent.putExtra("ID", cinemas.get(i).cinema_id);
				startActivity(intent);
			}
			
		});
		
		


	}
	//======================================================================================================
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle("More Options...");
		menu.add("View Map");
		menu.add("Dial Phone 1");
		menu.add("Dial Phone 2");
		menu.add("Go to Website");
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getTitle() == "View Map") {
			if (clickedCinema != null) {
				Intent mapIntent = null, chooser = null;
				mapIntent = new Intent(android.content.Intent.ACTION_VIEW);
				mapIntent.setData(Uri
						.parse("geo:" + clickedCinema.Gps_lat + ","
								+ clickedCinema.Gps_lng+ "?z="
								+ clickedCinema.Gps_zom));
				chooser = Intent.createChooser(mapIntent, "Launch Maps");
				startActivity(chooser);
			} else if (clickedGameHouse != null) {
				Intent mapIntent = null, chooser = null;
				mapIntent = new Intent(android.content.Intent.ACTION_VIEW);
				mapIntent.setData(Uri.parse("geo:" + clickedGameHouse.Gps_lat
						+ "," + clickedGameHouse.Gps_lng + "?z="
						+ clickedGameHouse.Gps_zom));
				chooser = Intent.createChooser(mapIntent, "Launch Maps");
				startActivity(chooser);
			} 
			return true;
		} else if (item.getTitle() == "Dial Phone 1") {
			if (clickedCinema != null) {
				Uri number = Uri
						.parse("tel:" + Uri.encode(clickedCinema.phone1));
				Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
				startActivity(callIntent);
			} else if (clickedGameHouse != null) {
				Uri number = Uri.parse("tel:"
						+ Uri.encode(clickedGameHouse.phone1));
				Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
				startActivity(callIntent);
			} 
			return true;
		} else if (item.getTitle() == "Dial Phone 2") {
			if (clickedCinema != null) {
				Uri number = Uri
						.parse("tel:" + Uri.encode(clickedCinema.phone2));
				Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
				startActivity(callIntent);
			} else if (clickedGameHouse != null) {
				Uri number = Uri.parse("tel:"
						+ Uri.encode(clickedGameHouse.phone2));
				Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
				startActivity(callIntent);
			} 
			return true;
		} else if (item.getTitle() == "Go to Website") {
			if (clickedCinema != null) {
				Uri webpage = Uri.parse(clickedCinema.website);
				Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
				startActivity(webIntent);
			} else if (clickedGameHouse != null) {
				Uri webpage = Uri.parse(clickedGameHouse.website);
				Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
				startActivity(webIntent);
			} 
			return true;
		} else {
			return false;
		}
	}
	
	/*=====================================================================*/
	
	private class cinemaAdapter extends ArrayAdapter<cinema> {

		private List<cinema> cinemas;

		public cinemaAdapter(Context context, int resource, int textViewResourceId,
				List<cinema> cinemas) {
			super(context, resource, textViewResourceId, cinemas);
			this.cinemas = cinemas;
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View row = inflator.inflate(R.layout.custom_list_1, parent, false);

			ImageView iv = (ImageView) row.findViewById(R.id.ivProfile1);
			TextView name = (TextView) row.findViewById(R.id.tvName1);
			TextView phone = (TextView) row.findViewById(R.id.tvPhone1);
			TextView mail = (TextView) row.findViewById(R.id.tvMail1);
			TextView address = (TextView) row.findViewById(R.id.tvAddress1);

			name.setText(cinemas.get(position).cinema_name);
			phone.setText(cinemas.get(position).phone1 + ", "
					+ cinemas.get(position).phone2);
			mail.setText(cinemas.get(position).email);
			address.setText(cinemas.get(position).address_desc);

			return row;
		}
	}
	
//===============================================================================================================
	
	private class gameHouseAdapter extends ArrayAdapter<gameHouse> {

		private List<gameHouse> gameHouses;

		public gameHouseAdapter(Context context, int resource, int textViewResourceId,
				List<gameHouse> gameHouses) {
			super(context, resource, textViewResourceId, gameHouses);
			this.gameHouses = gameHouses;
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View row = inflator.inflate(R.layout.custom_list_1, parent, false);

			ImageView iv = (ImageView) row.findViewById(R.id.ivProfile1);
			TextView name = (TextView) row.findViewById(R.id.tvName1);
			TextView phone = (TextView) row.findViewById(R.id.tvPhone1);
			TextView mail = (TextView) row.findViewById(R.id.tvMail1);
			TextView address = (TextView) row.findViewById(R.id.tvAddress1);

			name.setText(gameHouses.get(position).game_house_name);
			phone.setText(gameHouses.get(position).phone1 + ", "
					+ gameHouses.get(position).phone2);
			mail.setText(gameHouses.get(position).email);
			address.setText(gameHouses.get(position).address_desc);

			return row;
		}
	}
	
	//==============================================================================================================
	
	private class MyTask extends AsyncTask <String, String, String > {

		List<cinema> lcinemas;
		List<gameHouse> lgameHouses;
		
		@Override
		protected String doInBackground(String... arg0) {
			dbHelper db = new dbHelper(getBaseContext());

			lcinemas = db.getAllCinema();
			lgameHouses = db.getAllGameHouse();
			
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			
			updateView(lcinemas, lgameHouses);
			
		}

	}



	
	//===================================================================================================================

}
