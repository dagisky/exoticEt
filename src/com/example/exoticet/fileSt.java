package com.example.exoticet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.widget.Toast;

public class fileSt {

	String extDir;

	public fileSt(String rootFile) {
		// TODO Auto-generated constructor stub
		this.extDir = rootFile;
	}

	public boolean chkExternalStorage(Context context) {
		String state = Environment.getExternalStorageState();

		if (state.equals(Environment.MEDIA_MOUNTED)) {
			return true;
		} else if (state.equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
			Toast.makeText(context, "External Storage is read only!",
					Toast.LENGTH_LONG).show();
			return false;
		} else {
			Toast.makeText(context, "External Storage is not available!",
					Toast.LENGTH_LONG).show();
			return false;
		}
	}

	public String writeImgExt(FileOutputStream fos, Bitmap bitmap, String name)
			throws FileNotFoundException {

		File f = new File(name);

		if (f.exists()) {
			f.delete();

			try {
				bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
				fos.flush();
				fos.close();
				return f.getAbsolutePath();
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		} else {
			return f.getAbsolutePath();
		}

	}

	public boolean writeImgInt(FileOutputStream fos, Bitmap bitmap, String name)
			throws IOException {
		File file = new File(name);
		if (file.exists()) {
			file.delete();
		}
		if (bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos)) {
			fos.flush();
			fos.close();
			return true;
		} else {
			fos.flush();
			fos.close();
			return false;
		}
	}

}
