package com.example.exoticet;

import java.util.List;

import model.site;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;

public class Touristic_site_list extends Activity {

	ListView naturalList;
	ListView historicalList;
	ListView pplCultList;
	TabHost th;
	site clickedSite;

	public Touristic_site_list() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.touristic_site_list);

		th = (TabHost) findViewById(R.id.touristicTabHost);
		th.setup();
		

		naturalList = (ListView) findViewById(R.id.natlistView);
		historicalList = (ListView) findViewById(R.id.histlistView);
		pplCultList = (ListView) findViewById(R.id.pplClistView);
		
		MyTask fetchData = new MyTask();
		fetchData.execute();
			

	}

	private void updateView(final List<site> natSite, final List<site> histSite, final List<site> ppleCult) {
		
		if(natSite != null){
		MyAdapter adapter = new MyAdapter(getBaseContext(),
				android.R.layout.simple_list_item_1, R.id.tvName1, natSite);
		naturalList.setAdapter(adapter);
		}
		if(histSite != null){
		MyAdapter adapter2 = new MyAdapter(getBaseContext(),
				android.R.layout.simple_list_item_1, R.id.tvName1, histSite);
		historicalList.setAdapter(adapter2);
		}
		if(ppleCult != null){
		MyAdapter adapter3 = new MyAdapter(getBaseContext(),
				android.R.layout.simple_list_item_1, R.id.tvName1, ppleCult);
		pplCultList.setAdapter(adapter3);
		}
		
		TabSpec ts = th.newTabSpec("tag1");
		ts.setContent(R.id.natural);
		ts.setIndicator("Natural");
		th.addTab(ts);

		ts = th.newTabSpec("tag2");
		ts.setContent(R.id.hsitorical);
		ts.setIndicator("Historical");
		th.addTab(ts);

		ts = th.newTabSpec("tag3");
		ts.setContent(R.id.pplCul);
		ts.setIndicator("People and Culture");
		th.addTab(ts);
		
		registerForContextMenu(naturalList);
		registerForContextMenu(historicalList);
		registerForContextMenu(pplCultList);
		
		naturalList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View view,
					int i, long l) {
				// TODO Auto-generated method stub
				clickedSite = natSite.get(i);
				return false;
			}
		});
		
		historicalList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View view,
					int i, long l) {
				// TODO Auto-generated method stub
				clickedSite = histSite.get(i);
				return false;
			}
		});
		
		pplCultList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View view,
					int i, long l) {
				// TODO Auto-generated method stub
				clickedSite = ppleCult.get(i);
				return false;
			}
		});
		
		naturalList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int i,
					long l) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getBaseContext(), Detail_view.class);
				intent.putExtra("TYPE", "site");
				String site[] = {String.valueOf(natSite.get(i).site_id), natSite.get(i).site_name,
						natSite.get(i).profile_img, String.valueOf(natSite.get(i).Gps_lat), String.valueOf(natSite.get(i).GPS_long),
						String.valueOf(natSite.get(i).Gps_zoom), String.valueOf(natSite.get(i).site_category),natSite.get(i).site_category_type,
						natSite.get(i).small_desc, natSite.get(i).phone1, natSite.get(i).phone2,
						natSite.get(i).email, natSite.get(i).website, natSite.get(i).country, natSite.get(i).city, natSite.get(i).address_desc};
				intent.putExtra("site", site);
				
				startActivity(intent);
			}
		});
		
		historicalList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int i,
					long l) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getBaseContext(), Detail_view.class);
				intent.putExtra("TYPE", "site");
				String site[] = {String.valueOf(histSite.get(i).site_id), histSite.get(i).site_name,
						histSite.get(i).profile_img, String.valueOf(histSite.get(i).Gps_lat), String.valueOf(histSite.get(i).GPS_long),
						String.valueOf(natSite.get(i).Gps_zoom), String.valueOf(histSite.get(i).site_category),histSite.get(i).site_category_type,
						histSite.get(i).small_desc, histSite.get(i).phone1, histSite.get(i).phone2,
						histSite.get(i).email, histSite.get(i).website, histSite.get(i).country, histSite.get(i).city, histSite.get(i).address_desc};
				intent.putExtra("site", site);
				
				startActivity(intent);
				
			}
		});
		
		pplCultList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adpater, View view, int i,
					long l) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getBaseContext(), Detail_view.class);
				intent.putExtra("TYPE", "site");
				String site[] = {String.valueOf(ppleCult.get(i).site_id), ppleCult.get(i).site_name,
						ppleCult.get(i).profile_img, String.valueOf(ppleCult.get(i).Gps_lat), String.valueOf(ppleCult.get(i).GPS_long),
						String.valueOf(ppleCult.get(i).Gps_zoom), String.valueOf(ppleCult.get(i).site_category),ppleCult.get(i).site_category_type,
						ppleCult.get(i).small_desc, ppleCult.get(i).phone1, ppleCult.get(i).phone2,
						ppleCult.get(i).email, ppleCult.get(i).website, ppleCult.get(i).country, ppleCult.get(i).city, ppleCult.get(i).address_desc};
				intent.putExtra("site", site);
				
				startActivity(intent);
			}
		});

		
	}
	
	/*=========================================================================================*/
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle("More Options...");
		menu.add("View Map");
		menu.add("Dial Phone 1");
		menu.add("Dial Phone 2");
		menu.add("Go to Website");
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getTitle() == "View Map") {
			if (clickedSite != null) {
				Intent mapIntent = null, chooser = null;
				mapIntent = new Intent(android.content.Intent.ACTION_VIEW);
				mapIntent.setData(Uri
						.parse("geo:" + clickedSite.Gps_lat + ","
								+ clickedSite.GPS_long+ "?z="
								+ clickedSite.Gps_zoom));
				chooser = Intent.createChooser(mapIntent, "Launch Maps");
				startActivity(chooser);
			}  
			return true;
		} else if (item.getTitle() == "Dial Phone 1") {
			if (clickedSite != null) {
				Uri number = Uri
						.parse("tel:" + Uri.encode(clickedSite.phone1));
				Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
				startActivity(callIntent);
			} 
			return true;
		} else if (item.getTitle() == "Dial Phone 2") {
			if (clickedSite != null) {
				Uri number = Uri
						.parse("tel:" + Uri.encode(clickedSite.phone2));
				Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
				startActivity(callIntent);
			}  
			return true;
		} else if (item.getTitle() == "Go to Website") {
			if (clickedSite != null) {
				Uri webpage = Uri.parse(clickedSite.website);
				Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
				startActivity(webIntent);
			}  
			return true;
		} else {
			return false;
		}
	}
	
	/*=========================================================================================*/

	private class MyAdapter extends ArrayAdapter<site> {

		private List<site> sites;

		public MyAdapter(Context context, int resource, int textViewResourceId,
				List<site> sites) {
			super(context, resource, textViewResourceId, sites);
			this.sites = sites;
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View row = inflator.inflate(R.layout.custom_list_1, parent, false);

			ImageView iv = (ImageView) row.findViewById(R.id.ivProfile1);
			TextView name = (TextView) row.findViewById(R.id.tvName1);
			TextView phone = (TextView) row.findViewById(R.id.tvPhone1);
			TextView mail = (TextView) row.findViewById(R.id.tvMail1);
			TextView address = (TextView) row.findViewById(R.id.tvAddress1);
			

			name.setText(sites.get(position).site_name);
			phone.setText(sites.get(position).phone1 + ", "
					+ sites.get(position).phone2);
			mail.setText(sites.get(position).email);
			address.setText(sites.get(position).address_desc);
			

			return row;
		}
	}

	private class MyTask extends AsyncTask <String, String, String > {

		List<site> natural;
		List<site> historical;
		List<site> pplCult;
		
		@Override
		protected String doInBackground(String... arg0) {
			dbHelper db = new dbHelper(getBaseContext());

			natural = db.getAllSites(3);
			historical = db.getAllSites(1);
			pplCult = db.getAllSites(2);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			
			updateView(natural, historical, pplCult);
			
		}

	}

}
