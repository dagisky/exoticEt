package com.example.exoticet;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

public class Setting extends Activity implements OnClickListener{
	
	TabHost th;
	Button btnUpdateSites;
	Button btnUpdateAcco;
	Button btnUpdateEnter;
	ProgressBar pb;
	TextView txtSqlData;

		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);	
		
		btnUpdateSites = (Button) findViewById(R.id.btnUpdateSite);
		btnUpdateSites.setOnClickListener(this);
		btnUpdateAcco = (Button) findViewById(R.id.btnUpdateAcc);
		btnUpdateAcco.setOnClickListener(this);
		btnUpdateEnter = (Button) findViewById(R.id.btnUpdateEntertain);
		btnUpdateEnter.setOnClickListener(this);
		
		txtSqlData = (TextView) findViewById(R.id.tvCheck);
		
		pb = (ProgressBar) findViewById(R.id.pbSetting);
		pb.setVisibility(View.INVISIBLE);
		
			
	
	}
	
	public void updateView(String trial){
		txtSqlData.setText(trial);
	}
	

	
	protected boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if(netInfo != null && netInfo.isConnectedOrConnecting()){
			return true;
		}else{
			return false; 
		}
	}
	
	private void updateData(String uri) {		
		MyTask updateTask = new MyTask();
		updateTask.execute(uri);	
		
	}
	
	private void updateDb(String data) {
		dbHelper db = new dbHelper(getBaseContext());
		String param = ";";
		String queryArray[] = data.split(param);
		db.updatedb(queryArray);
	}
	
	private class MyTask extends AsyncTask <String, String, String > {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pb.setVisibility(View.VISIBLE);
		}
		
		@Override
		protected String doInBackground(String... params) {
			String content = HttpManager.getData(params[0]);
			updateDb(content);
			return content;
		}

		@Override
		protected void onPostExecute(String result) {
			pb.setVisibility(View.INVISIBLE);
			updateView(result);
			
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.btnUpdateSite){
			if(isOnline()){
				updateData("http://abugidatemari.net/Resources/site.sql");
			}else{
				Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
			}
		}else if(v.getId() == R.id.btnUpdateAcc){
			if(isOnline()){
				updateData("http://abugidatemari.net/Resources/acco.sql");
			}else{
				Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
			}
		}else if(v.getId() == R.id.btnUpdateEntertain){
			if(isOnline()){
				updateData("http://abugidatemari.net/Resources/ent.sql");
			}else{
				Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
			}
		}
	}

	

}
